

$(function() {
	try{Typekit.load();}catch(e){} //para pegar a tipografia fora do servidor

	$('.dropdown-hover').dropdownHover();
	$('.activate-tooltip').tooltip();

$("#treeview").treeview({
	persist: "location",
	collapsed: true
});

 $('.site-container').jfontsize();
});

// Procura Google

(function() {
  var cx = '001431837936789342560:4ubhf4ret5o';
  var gcse = document.createElement('script');
  gcse.type = 'text/javascript';
  gcse.async = true;
  gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
      '//www.google.com/cse/cse.js?cx=' + cx;
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(gcse, s);
})(); 

function onProcura(){
	var str = $("#procura").val().trim();	
	var caixaGoogle = $(".gsc-input");
	if(caixaGoogle.length== 0){
		alert("Interface da Google não foi encontrada. Aguarde um pouco e tente novamente. Se o problema persistir, atualize a página.");
	}else{
		//$(".gsc-search-button").hide();
		caixaGoogle.val(str);
		$(".gsc-search-button").click();
	}
	
}

function onProcuraClick(event){
    if(event.keyCode == 13){
    	onProcura();
    }
}


