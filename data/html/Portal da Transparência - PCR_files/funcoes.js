
MASCARA_DATA = "99/99/9999";
MASCARA_CPF = "999.999.999-99";
MASCARA_CNPJ = "99.999.999/9999-99";

CHARS_STRING_UPCASE =  "ABCDEFGHIJKLMNOPQRSTUVWXYZÇÁÉÍÓÚÀÈÌÒÙÃÕÂÔÛ ";
CHARS_STRING_LOWCASE = "abcdefghijklmnopqrstuvwxyzçáéíóúàèìòùãõâôû ";
CHARS_INTEIRO = "0123456789";
CHARS_SIMBOLO = "'!@#$%¨&*(),:}{^~/\\\"<>;[]{}§´`'º°ª";

/** Evoca uma função se ela existe */
function ifFnExistsCallIt(fnName){
	   fn = window[fnName];
	   fnExists = typeof fn === 'function';
	   if(fnExists)
	      fn();
}
/** Remove qualquer caractere que não é espaço ou alfanumérico.  */
function removerSimbolos(string){
	//alert(string);
	nova_string = string.replace(/[^A-Za-z0-9]/g, '');
	//alert(nova_string);
	
	return nova_string;
}
/** formata um campo de acordo com a máscara informada.  */
function formatar(mascara, campo){
	var i = campo.value.length;
	var saida = mascara.substring(0,1);
	var texto = mascara.substring(i)

  if (texto.substring(0,1) != saida){
		campo.value += texto.substring(0,1);
  }
}
/** Converte um valor de um campo texto para moeda.  */
function MascaraMoeda(objTextBox, e){
	var SeparadorMilesimo = '.';
	var SeparadorDecimal = ',';
	var sep = 0;
	var key = '';
	var i = j = 0;
	var len = len2 = 0;
	var strCheck = '0123456789';
	var aux = aux2 = '';
	var whichCode = (window.Event) ? e.which : e.keyCode;
	if (whichCode == 13) return true;
	if (whichCode == 8) return true;
	key = String.fromCharCode(whichCode); // Valor para o código da Chave
	if (strCheck.indexOf(key) == -1) return false; // Chave inválida
	len = objTextBox.value.length;
	for(i = 0; i < len; i++)
		if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
	aux = '';
	for(; i < len; i++)
		if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
	aux += key;
	len = aux.length;
	if (len == 0) objTextBox.value = '';
	if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
	if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
	if (len > 2) {
    aux2 = '';
    for (j = 0, i = len - 3; i >= 0; i--) {
	    if (j == 3) {
	      aux2 += SeparadorMilesimo;
	      j = 0;
	    }
	    aux2 += aux.charAt(i);
	    j++;
    }
    objTextBox.value = '';
    len2 = aux2.length;
    for (i = len2 - 1; i >= 0; i--)
    objTextBox.value += aux2.charAt(i);
    objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
	}
	return false;
}

/** Adiciona uma mensagem de SUCESSO no DIV informado.  
 * @param idDivErro ID do DIV que receberá as mensagens de erro, aviso e sucesso
 * @param mensagem Mensagem para ser apresentada ao usuário
 * */
function addMsgSucesso(idDivErro, mensagem){
	  document.getElementById(idDivErro).innerHTML = '<a name="'+idDivErro+'"></a><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'+mensagem+'</div>';
	  this.location = "#"+idDivErro;
}

/** Adiciona uma mensagem de ERRO no DIV informado.  
 * @param idDivErro ID do DIV que receberá as mensagens de erro, aviso e sucesso
 * @param mensagem Mensagem para ser apresentada ao usuário
 * */
function addMsgErro(idDivErro, mensagem){
	  document.getElementById(idDivErro).innerHTML = '<a name="'+idDivErro+'"></a><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>'+mensagem+'</div>';
	  this.location = "#"+idDivErro;
}
/** Adiciona uma mensagem de AVISO no DIV informado.  
 * @param idDivErro ID do DIV que receberá as mensagens de erro, aviso e sucesso
 * @param mensagem Mensagem para ser apresentada ao usuário
 * */
function addMsgAviso(idDivErro, mensagem){
	  document.getElementById(idDivErro).innerHTML = '<a name="'+idDivErro+'"></a><div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">×</button>'+mensagem+'</div>';
	  this.location = "#"+idDivErro;
}
/** Limpa o conteúdo de um div.  */
function cleanMsg(idDivErro){
	document.getElementById(idDivErro).innerHTML = '';
}

// Adicionando recursos de trim no protótipo das strings
if (!String.prototype.trim) {
	String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};

	String.prototype.ltrim=function(){return this.replace(/^\s+/,'');};

	String.prototype.rtrim=function(){return this.replace(/\s+$/,'');};

	String.prototype.fulltrim=function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};
}

/** Criando um objeto para manipulação de Data.  */
function Data(data){
	this.setData(data);
}
Data.prototype.setData = function(datatxt){
    var dataForm = (datatxt).split("/");
    dataForm[0] = parseInt(dataForm[0],10);
    x= dataForm[1];
    dataForm[1] = parseInt(dataForm[1],10);
    dataForm[2] = parseInt(dataForm[2],10);
    data = new Date(dataForm[2], (dataForm[1]-1), dataForm[0]);
    if(!data){
		//txt não é uma data
		data=null;
		throw "Data inválida.";    	
    }else if(data.getDate()!=dataForm[0] || data.getMonth()!=dataForm[1]-1 || data.getFullYear()!=dataForm[2] ){
    	//txt é uma data inválida
    	data=null;
    	throw "Data inválida.";
    }
    this.data = data;
}
Data.prototype.getTime = function(){
	return this.data.getTime();
}
// Instantiate new objects with 'new'

/** Abre uma janela popup.  */
function openWindow(url,largura,altura,janela) {
	window.open(url,janela,'location=no,menubar=no,toolbar=yes,status=yes,resizable=yes,scrollbars=yes,left=50,top=40,width='+largura+',height='+altura+',resizable=yes');
}

/**
 * Retorna a string com todas as ocorrências de token substituídas por newtoken
 * */
function replaceAll(string, token, newtoken) {
	while (string.indexOf(token) != -1) {
 		string = string.replace(token, newtoken);
	}
	return string;
}

scriptIDcount = 0;

/** seta máscaras padrão nas classes apropriadas.  */
function setMascarasPadrao(){
	  $(".dataCaixa").datepicker( $.datepicker.regional[ "pt-BR" ]);
	  $(".dataCaixa").mask(MASCARA_DATA,{placeholder:"_"});
	  $(".data").mask(MASCARA_DATA,{placeholder:"_"});
	  $(".cpf").mask(MASCARA_CPF,{placeholder:"_"});
	  $(".cnpj").mask(MASCARA_CNPJ,{placeholder:"_"});
	  $(".numero").numeric();
	  $('.apenasNumeros').keyup(function () {
		    if (this.value != this.value.replace(/[^0-9]/g, '')) {
		       this.value = this.value.replace(/[^0-9]/g, '');
		    }
	  });
	
}

/**
 * Verifica os campos obrigatórios vazios de um <form>
 * É necessário colocar a classe do campo como 'campo_obrigatorio' e adicionar dentro da tag o parâmetro label='Descrição do campo'.
 * Exemplo: 
	 antes de definir o item como obrigatório:  
		 <input type="text" class="span12" id="titulo" name="titulo" value=""></input>
	 depois de definir como obrigatório: 
		 <input type="text" class="span12 campo_obrigatorio" label="Título" id="titulo" name="titulo" value=""></input>
 * @param idDivMensagem ID do <DIV> que receberá as mensagens de falha
 * @return FALSE se a checagem falhou. TRUE se tudo está preenchido
 **/
function checarCamposObrigatoriosVazios(idDivMensagem){
	msgErro="";
	camposVazios="";
	$('.campo_obrigatorio').each(
	    function(index){  
	      var input = $(this);
	      if(input.val()==""){
	    	  if(camposVazios!=""){
	    		  camposVazios +=", ";
	    	  }
	    		camposVazios += "'"+input.attr('label')+"'";
	    	}
	    }
	);
	if(camposVazios!=""){
		msgErro = "Os campo(s) "+camposVazios+" são obrigatório(s).";
		addMsgErro(idDivMensagem, msgErro);
	}
	if(msgErro){
		return false;
	}else{
		return true;
	}
}

/**
 * Esta função deve ser chamada para dar submit no formulário. Ele verifica por falhas no preenchimento padrão antes de submeter.
 * @param formulario Formulário a ser dado o submit
 * @param idDivMensagem ID do <DIV> que receberá as mensagens de falha
 * @return FALSE se a checagem falhou. TRUE se tudo está preenchido
 **/
function onClick_submeterFormulario(formulario,idDivMensagem){
	if(checarCamposObrigatoriosVazios(idDivMensagem)){
		$('#'+formulario).submit();
	}
}


/** OBSOLETO - Usar o JQuery $().load() ou $().ajax() 
 * Chamada de ajax.  */
function loadScript(url, callback){
    // adding the script tag to the head as suggested before
   var head = document.getElementsByTagName('head')[0];
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = url;

   // then bind the event to the callback function 
   // there are several events for cross browser compatibility
   script.onreadystatechange = callback;
   script.onload = callback;

   // fire the loading
   head.appendChild(script);
}

$(function() {
  // Seta valores do datepicker para a língua portuguesa	
  $.datepicker.regional['pt-BR'] = {
	closeText: 'Fechar',
	prevText: '&#x3c;Anterior',
	nextText: 'Pr&oacute;ximo&#x3e;',
	currentText: 'Hoje',
	monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
	'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
	'Jul','Ago','Set','Out','Nov','Dez'],
	dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
	dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
	dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
	weekHeader: 'Sm',
	dateFormat: 'dd/mm/yy',
	firstDay: 0,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''
  };
  //seta máscaras padrão
  setMascarasPadrao();
});
