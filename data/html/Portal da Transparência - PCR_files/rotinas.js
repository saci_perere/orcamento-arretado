﻿//atribuindo ao evento onLoad a função inicializa automaticamente...
//window.onload = inicia_funcoes;

var	valida_nivel2 = null;

function inicia_funcoes()
{
	try
	{
		inicializa();
	}
	catch(e)
	{
	
	}
}

function inicializa() {
	var qtdForm = document.forms.length;
	for (var indForm = 0; indForm < qtdForm; indForm++) {
		var tamanho = document.forms[indForm].elements.length;
		var i;
		for (i = 0; i < tamanho; i++) {
			if (document.forms[indForm].elements[i].tagName.toUpperCase() != "FIELDSET") {
				if ( (document.forms[indForm].elements[i].name.toUpperCase().indexOf("DATA") >= 0) || (document.forms[indForm].elements[i].getAttribute("date") != null) ) {
					document.forms[indForm].elements[i].onblur = function(event)
						{ formata_ano(this) }
					if(navigator.appName=="Netscape") {
						document.forms[indForm].elements[i].onkeypress = function(event) 
							{ return formata_data(this, event) }
					} else {
						document.forms[indForm].elements[i].onkeypress = function(event)
							{ return formata_data(this, window.event) }
					}//if
				} else if ( (document.forms[indForm].elements[i].name.substr(document.forms[indForm].elements[i].name.length -2) == '_$') || (document.forms[indForm].elements[i].getAttribute("money")  != null) ) {
					if (trim(document.forms[indForm].elements[i].value) != '')
						Formata_Valores(document.forms[indForm].elements[i], '$');
						document.forms[indForm].elements[i].onblur = function(event)
						{ Formata_Valores(this, '$') }
				} else if ( (document.forms[indForm].elements[i].name.substr(document.forms[indForm].elements[i].name.length -2).toUpperCase() == '_N') || (document.forms[indForm].elements[i].getAttribute("numeric") != null) ) {
					if (trim(document.forms[indForm].elements[i].value) != '')
						Formata_Valores(document.forms[indForm].elements[i], 'N');
							document.forms[indForm].elements[i].onblur = function(event)
							{ Formata_Valores(this, 'N') }
				} else {
					if ((document.forms[indForm].elements[i].type == "submit"))	{
						//document.forms[indForm].elements[i].onclick = function(event)
						//{return valida_campos(this.form) }
					} else{
						if ((document.forms[indForm].elements[i].name.toUpperCase().indexOf("SUBMIT")) >= 0) {
							document.forms[indForm].elements[i].onclick = function(event)
							{valida_campos(this.form) }
						}//if
					}//if
				}//if
			}//if
		}//for dos campos
	}//for dos forms
}//Inicializa

function formata_data(campo, teclapress){
	var tam = campo.value.length;
	var charCode = (navigator.appName == "Netscape") ? teclapress.which : teclapress.keyCode;
	//só permite digitar números ou a barra.
	if (charCode == 0){
		return true;
	}//if
	if(charCode > 31 && (charCode < 47 || charCode > 57)) 
		return false;
	//apaga o conteudo caso use o backspace no netscape
	if(navigator.appName == "Netscape" && tam == 10 && charCode == 8 )
	{
		campo.value = '';
		return false;
	}//if
	if (charCode != 8 && tam < 10 && charCode != 47)
	{
		if (tam==0 && parseInt(String.fromCharCode(charCode)) >= 4)
			campo.value = '0' + campo.value + String.fromCharCode(charCode) + '/';
		else if (tam==1)
			campo.value = campo.value + String.fromCharCode(charCode) + '/';
		else if(tam==2 && campo.value.substr(3, 1) != '/')
			campo.value = campo.value + '/' + String.fromCharCode(charCode);
		else if (tam==3 && parseInt(String.fromCharCode(charCode)) >= 2)
			campo.value = campo.value + '0' + String.fromCharCode(charCode) + '/';
		else if (tam==4)
			campo.value = campo.value + String.fromCharCode(charCode) + '/';
		else if(tam==5 && campo.value.substr(6, 1) != '/')
			campo.value = campo.value + '/' + String.fromCharCode(charCode);
		else if(navigator.appName == "Netscape" && tam < 10)
			campo.value = campo.value + String.fromCharCode(charCode);
		else
			return true;
		return false;
	}//if
}//formatadata

function formata_ano(campo)
{
	var tam = campo.value.length;
	if (tam >= 6)
	{
		campo.value = replace(campo.value, '/', '');
		campo.value = campo.value.substr(0, 2)+'/'+campo.value.substr(2, 2)+'/'+campo.value.substr(4, 4);
		tam = campo.value.length;
	}//if
	if (tam < 10)
	{
		if (tam == 7)
		{
			var ano_antigo = "190";
			var ano_novo   = "200";			
		}
		else if (tam == 8 || tam == 9)
		{
			var ano_antigo = "19";
			var ano_novo   = "20";			
		}//if
		if (campo.name.substr(campo.name.length -1).toUpperCase() == 'P')
		{
			if (parseInt((campo.value.substr(6, 2))) < 5)
				campo.value = campo.value.substr(0, 6) + ano_novo + (campo.value.substr(6, 2));
			else if (parseInt((campo.value.substr(6, 2))) >= 5)
				campo.value = campo.value.substr(0, 6) + ano_antigo + (campo.value.substr(6, 2));
		}
		else	if (campo.name.substr(campo.name.length -1).toUpperCase() == 'F')
		{
			if (parseInt((campo.value.substr(6, 2))) < 95)
				campo.value = campo.value.substr(0, 6) + ano_novo + (campo.value.substr(6, 2));
			else if (parseInt((campo.value.substr(6, 2))) >= 95)
				campo.value = campo.value.substr(0, 6) + ano_antigo + (campo.value.substr(6, 2));
		}
		else
		{
			if (parseInt((campo.value.substr(6, 2))) < 80)
				campo.value = campo.value.substr(0, 6) + ano_novo + (campo.value.substr(6, 2));
			else if (parseInt((campo.value.substr(6, 2))) >= 80)
				campo.value = campo.value.substr(0, 6) + ano_antigo + (campo.value.substr(6, 2));
		}//if
	}//if
}//formata_ano

function valida_cnpj(cnpj)
{
	var Numero = cnpj;
	while (Numero.indexOf(".") != -1) 
		Numero = Numero.replace(".", ""); 
	Numero = Numero.replace("-", ""); Numero = Numero.replace("/", "");
	if (isNaN(Numero) == true) 
		return false;
	if (Numero.length != 14) 
		return false;
	var soma = 0
	var soma1 = 0
	var soma2 = 0
	var peso1 = 5
	var peso2 = 6
	var dig = 0		
	for (i = 0; i < 13; i++)
	{
		soma2 += parseInt(Numero.substr(i,1)) * peso2
		if (i != 12) soma1 += parseInt(Numero.substr(i,1)) * peso1
		peso1--;
		peso2--;
		if (peso1 < 2) peso1 = 9
		if (peso2 < 2) peso2 = 9
	}//for
	soma = soma1
	var r = (soma % 11)
	if ((r == 0) || (r == 1)) dig = 0
	else dig = 11 - r
	var dig1 = dig
	soma = soma2
	r = (soma % 11)
	if ((r == 0) || (r == 1)) dig = 0
	else dig = 11 - r
	var dig2 = dig
	if ((dig1 != parseInt(Numero.substr(12,1))) || (dig2 != parseInt(Numero.substr(13,1)))) 
		return false;
	else 
		return true;
}//valida_cnpj

function valida_cpf(cpf)
{
	var Numero = cpf;
	while (Numero.indexOf(".") != -1) 
		Numero = Numero.replace(".", ""); 
	Numero = Numero.replace("-", "");
	if (isNaN(Numero) == true) 
		return false;
	if (Numero.length != 11) 
		return false;
	var Soma = 0
	var contador1 = 10
	for (i = 0; i < 9; i++)
	{
		var Caracter = parseInt(Numero.substr(i,1))
		Soma += Caracter * contador1
		contador1--;
	}//for
	var Resto = (Soma % 11)
  if ((Resto == 0) || (Resto == 1)) 
		var Valor10 = 0;
  else 
		var Valor10 = 11 - Resto;
	if (Valor10 != parseInt(Numero.substr(9,1))) 
		return false;
	var Contador2 = 11
	var Soma = 0
	for (i = 0; i < 10; i++)
	{
		var Caracter = parseInt(Numero.substr(i,1))
		Soma += Caracter * Contador2
		Contador2--
	}//for
	Resto = (Soma % 11)
  if ((Resto == 0) || (Resto == 1)) 
		Valor11 = 0;
  else 
		Valor11 = 11 - Resto;
	if (Valor11 != parseInt(Numero.substr(10,1))) 
		return false;
	else 
		return true;
}//valida_cpf	

function valida_data(datateste, tipodata)
{
	if(datateste.length != 10) 
		return false;
	var datateste = replace(datateste,"-","/");
	var delim1 = datateste.indexOf("/");
	var delim2 = datateste.lastIndexOf("/");
	if (delim1 != -1 && delim1 == delim2) 
		return false;
	if (delim1 != -1)
	{
		var dd = parseInt(datateste.substr(0,delim1),10);
		var mm = parseInt(datateste.substr(delim1 + 1,delim2),10);
		var yyyy = parseInt(datateste.substr(delim2 + 1, datateste.length),10);
	}
	else
	{
		var dd = parseInt(datateste.substr(0,2),10);
		var mm = parseInt(datateste.substr(2,4),10);
		var yyyy = parseInt(datateste.substr(4,datateste.length),10);
	}//if
	if (isNaN(mm) || isNaN(dd) || isNaN(yyyy)) 
		return false;
	/***** INICIO CHECAGEM DE ANO *****/
	if (yyyy < 1789 || yyyy > 2300) 
		return false;
	/***** FINAL CHECAGEM DE ANO *****/
	
	/***** INICIO CHECAGEM DE MES *****/
	if (mm < 1 || mm > 12) 
		return false;
	if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && dd > 30) 
		return false;
	else if (dd > 31) 
		return false;
	/***** FINAL CHECAGEM DE MES *****/

	/***** INICIO CHECAGEM DE DIA *****/
	if (dd < 1 || dd > 31) 
		return false;
	if(mm==2)
	{
		if (yyyy % 4 > 0 && dd > 28) 
			return false;
		else if (dd > 29) 
			return false;
	}//if
	/***** FINAL CHECAGEM DE DIA *****/
	
	/***** INICIO CHECAGEM DE PERIODO *****/
	var dataobj = new Date();
	var ddhoje=dataobj.getDate();
	if(ddhoje.toString().length < 2) ddhoje='0'+ddhoje;
	var mmhoje=dataobj.getMonth()+1;
	if(mmhoje.toString().length < 2) mmhoje='0'+mmhoje;
	var yyyyhoje=dataobj.getYear();
	if((yyyyhoje.toString()).length < 4) yyyyhoje = yyyyhoje + 1900;
	
	var idd=dd.toString();
	var imm=mm.toString();
	
	if(idd.length < 2) 
		idd='0'+idd;
	if(imm.length < 2) 
		imm='0'+imm;
	
	var idatateste = yyyy+''+imm+''+idd;
	var idatahoje=yyyyhoje+''+mmhoje+''+ddhoje;

	if(tipodata.toUpperCase()=='F') 
	{
		if(parseInt(idatateste) < parseInt(idatahoje)) 
			return false;
	}
	else
	{
		if(tipodata.toUpperCase()=='P') 
		{
			if(parseInt(idatateste) > parseInt(idatahoje)) 
				return false;
		}//if
	}//if
	/***** FINAL CHECAGEM DE PERIODO *****/
	return true;
}//valida_data

function valida_email(campo)
{
	var Contador = 0
	var TotalCar = campo.length
	var arroba = 0
	var ponto = 0
	while (Contador < TotalCar)
	{
		var Char = campo.toLowerCase().charCodeAt(Contador)
		if ((Char != 45) && (Char != 95) && (Char != 64) && (Char != 46) && ((Char < 48) || (Char > 57)) && ((Char < 97) || (Char > 122)))
		{
			return false;
			break;
		}//if
		else 
		{ 
			if (Char == 46) { ponto++ }
			if (Char == 64) { arroba++; ponto = 0; }
		}//else
		Contador++		
	}//while
	if ((ponto < 1) || (arroba != 1)) 
		return false; 
	else
		return true;
}//valida_email

function valida_campos(forms) {
	var mensagem = "Os seguintes campos não foram corretamente preenchidos:\n";
	var tamanho  = forms.elements.length;
	var erro     = 0;
	var erro_old = 0;
	var i, j;
	var nomecampo, nometitle, nomeCampoExibir;
	var cmd_foco = 0;
	var msgext;
	
	for (i = 0; i < tamanho; i++) {
		if (forms.elements[i].tagName.toUpperCase() != "FIELDSET"){
							
			nomecampo = trim(replace(forms.elements[i].name.toLowerCase(), '_', ' '));
			nomeCampoExibir = nomecampo.toUpperCase();
			
			/*** ALTERACAO POR LAF EM 05/11/2004 PARA PEGAR MENSAGEM EM OUTRA LINGUA ***/
			if(nomecampo=='rotinas msgempty') {
				msgext=forms.elements[i].value;
				if(msgext != '') mensagem=msgext+'\n';
			}//if
			/*** ALTERACAO POR LAF EM 05/11/2004 PARA PEGAR MENSAGEM EM OUTRA LINGUA ***/
	
			/*** ALTERACAO POR LAF EM 05/11/2004 PARA PEGAR O TITLE DO CAMPO SE DISPONÍVEL ***/
			nometitle=trim(forms.elements[i].title);
			if(nometitle != '') nomeCampoExibir=nometitle;
			/*** ALTERACAO POR LAF EM 05/11/2004 PARA PEGAR O TITLE DO CAMPO SE DISPONÍVEL ***/
			
			if(forms.elements[i].type == "text" || forms.elements[i].type == "password" || forms.elements[i].type == "textarea") { 
				/**** VALIDAR O CONTEUDO ****/
				if (trim(forms.elements[i].value).length > 0) {
					var funcao = "";
					var msg = "";
					if ((forms.elements[i].getAttribute("cpf") != null) || (forms.elements[i].getAttribute("cnpj") != null) ) {
						valor = replace(forms.elements[i].value, ".", "");
						if (valor.length >= 14) {
							funcao = ("valida_cnpj" + "('" + replace(forms.elements[i].value, "'", "") + "')");
							msg = "- CNPJ Inválido \n";					
						} else {
							funcao = ("valida_cpf" + "('" + replace(forms.elements[i].value, "'", "") + "')");
							msg = "- CPF Inválido \n";					
						}//if
					} else if ( (nomecampo.indexOf("email") >= 0) || (forms.elements[i].getAttribute("email") != null) ) {
						funcao = ("valida_email" + "('" + replace(forms.elements[i].value, "'", "") + "')");
						msg = "- "+ nomeCampoExibir + " Inválido \n";
					} else if ( (nomecampo.indexOf("data") >= 0) || (forms.elements[i].getAttribute("date")!= null) ) {
						var tipodata = nomecampo.substr(nomecampo.length - 2);
						funcao = ("valida_data" + "('" + replace(forms.elements[i].value, "'", "") + "', '"+ tipodata.substr(1) +"')");
						if (tipodata == ' p' || tipodata == ' f')
							if (nometitle == ""){
								msg = "- " + nomecampo.substr(0, (nomecampo.length - 2)).toUpperCase() + " Inválida \n";
							}else{
								msg = "- " + nomeCampoExibir + " Inválida \n";
							}//if
						else 
							msg = "- "+ nomeCampoExibir + " Inválida \n";
					} else if ( (nomecampo.substr(nomecampo.length -2).toUpperCase() == ' N') || (forms.elements[i].getAttribute("numeric") != null) ) {
						if (trim(forms.elements[i].value) != ''){
							funcao = "isNumeric(forms.elements[" + i + "])";
							msg = "- "+ nomeCampoExibir + " precisa ser numérico\n";
						}//if
					} else if ( (nomecampo.substr(nomecampo.length -2).toUpperCase() == ' $') || (forms.elements[i].getAttribute("money") != null) ) {
						if (trim(forms.elements[i].value) != ''){
							funcao = "isMoney(forms.elements[" + i + "])";
							msg = "- "+ nomeCampoExibir + " precisa ser numérico\n";
						}//if
					}//if
					if (funcao != "") {
						if (eval(funcao) == false) {
							mensagem = mensagem + msg;
							erro = 1; 
						}//if  
					}//if
				} else  {
					/**** VALIDAR CAMPOS OBRIGATÓRIOS ****/
					if ( (forms.elements[i].name.substr(0, 1) == "_") || (forms.elements[i].getAttribute("required") != null) || (forms.elements[i].getAttribute("req") != null)) {
						var tipodata = nomecampo.substr(nomecampo.length - 2);
						if ( (nomecampo.indexOf("data") >= 0) || (forms.elements[i].getAttribute("date") != null) ) {
							if(tipodata == ' p' || tipodata == ' f'){ 
								if (nometitle == ""){
									mensagem = mensagem + "- "+ nomecampo.substr(0, (nomecampo.length - 2)).toUpperCase() + "\n";
								}else{
									mensagem = mensagem + "- "+ nomeCampoExibir + "\n";
								}//if
							}else{
								mensagem = mensagem + "- "+ nomeCampoExibir + "\n";
							}//if
						} else if ( (tipodata == ' $') || (forms.elements[i].getAttribute("money") != null) ){
							if (nometitle == ""){
								mensagem+= "- "+ nomecampo.substr(0, (nomecampo.length - 2)).toUpperCase() + " Inválido \n";
							}else{
								mensagem+= "- "+ nomeCampoExibir + " Inválido \n";
							}//if
						} else if ( (tipodata == ' N') || (forms.elements[i].getAttribute("numeric") != null) ) {
							if (nometitle == ""){
								mensagem+= "- "+ nomecampo.substr(0, (nomecampo.length - 2)).toUpperCase() + " Inválido \n";
							}else{
								mensagem+= "- "+ nomeCampoExibir + " Inválido \n";
							}//if
						} else {
							if (nometitle == ""){
								mensagem = mensagem + "- "+ nomecampo.toUpperCase() + "\n";
							}else{
								mensagem = mensagem + "- "+ nomeCampoExibir + "\n";
							}//if
						}//if
						erro = 1; 
					}//if
				}//if
			} else if ( (forms.elements[i].name.substr(0, 1) == "_") || (forms.elements[i].getAttribute("required") != null) || (forms.elements[i].getAttribute("req") != null)) {
				if(forms.elements[i].type == "radio" || forms.elements[i].type == "checkbox") {
					var nome = forms.elements[i].name;
					var tam  = forms.elements[nome].length;
					var erro2 = 1;
					if (tam > 1){
						for (j = 0; j < tam; j++){
							if (forms.elements[nome][j].checked){
								erro2 = 0;
							}//if
							i ++;
						}//for
					}else{
						if (forms.elements[nome].checked){
							erro2 = 0;
						}//if
					}//if
					if (erro2 == 1)	{
						if (nometitle == ""){
							mensagem = mensagem + "- "+ nomecampo.toUpperCase() + "\n";
						}else{
							mensagem = mensagem + "- "+ nomeCampoExibir + "\n";
						}//if
						erro = 1;
					}//if
				}else if(forms.elements[i].type == "select-one"){
					if (forms.elements[i].selectedIndex == 0){
						if (nometitle == ""){
							mensagem = mensagem + "- "+ nomecampo.toUpperCase() + "\n";
						}else{
							mensagem = mensagem + "- "+ nomeCampoExibir + "\n";
						}//if
						erro = 1;
					}//if
				}else if(forms.elements[i].type == "select-multiple"){
					if (forms.elements[i].selectedIndex == -1){
						if (nometitle == ""){
							mensagem = mensagem + "- "+ nomecampo.toUpperCase() + "\n";
						}else{
							mensagem = mensagem + "- "+ nomeCampoExibir + "\n";
						}//if
						erro = 1;
					}//if
				}//if		
			}//if
			if(erro_old == 0 && erro == 1) {
				cmd_foco = i;
			}//if
			erro_old = erro;
		}//if
	}//for	

	if (erro == 0) {
		if (valida_nivel2 != null) {
			if (valida_nivel2(forms) == true) {
				forms.submit();
			} else {
				return false;
			}//if
		} else {
			forms.submit();
		}//if
	} else if (erro == 1) {
		alert(mensagem);
		forms.elements[cmd_foco].focus();
		return false;
	}//if
}//valida_campos

function trim(campo) {
	contador = 0
	while (contador < campo.length) {
		if (campo.charCodeAt(0) == 32) 
			campo = campo.substr(1,campo.length);
		else 
			break; 
		contador++;
	}//while
	contador = campo.length
	while (contador > 0) {
		if (campo.charCodeAt(campo.length - 1) == 32) 
			campo = campo.substr(0,campo.length - 1)
		else 
			break;
		contador--;
	}//while
	return campo;
}//trim

function replace(texto, antigo, novo)
{
  var t_resto   = texto; 
  var t_retorno = ''; 
  var pos_ini   = 0; 
  var pos       = t_resto.indexOf(antigo);
  while(pos > -1) 
	{
    pos_ini   = pos + novo.length;
		if (navigator.appName != "Netscape")
	    t_resto = t_resto.replace(antigo, novo); 
		else
			t_resto = t_resto.substr(0, pos) + novo + t_resto.substr(pos + antigo.length);
		t_retorno = t_retorno + t_resto.substr(0, pos_ini);
		t_resto   = t_resto.substr(pos_ini); 
		pos       = t_resto.indexOf(antigo); 
  }//while
  t_retorno = t_retorno + t_resto; 
  return t_retorno; 
}//replace

function WriteDate()
{
  hoje = new Date()
  dia  = hoje.getDate()
  dias = hoje.getDay()
  mes  = hoje.getMonth()
  ano  = hoje.getYear()
//  if (dia < 10)
//		dia = "0" + dia
	if (navigator.appName == "Netscape")
	{
		if (navigator.appVersion >= "4.5" )
		{
			if (ano < 100)
				ano = "19" + ano;
			else 
				ano = (1900 + ano)
		}//if
	}//if
	else if (navigator.appName == "Microsoft Internet Explorer")
	{
		if (ano < 2000)
			ano = "19" + ano
	}//if
	function CriaArray(n)
	{
		this.length = n 
	}//CriaArray
	NomeDia = new CriaArray(7); 
	NomeDia[0] = "Domingo"; NomeDia[1] = "Segunda-feira";
	NomeDia[2] = "Ter&ccedil;a-feira"; NomeDia[3] = "Quarta-feira"; NomeDia[4] = "Quinta-feira";
	NomeDia[5] = "Sexta-feira"; NomeDia[6] = "S&aacute;bado"; 
	NomeMes = new CriaArray(12);
	NomeMes[0] = "Janeiro"; NomeMes[1] = "Fevereiro"; NomeMes[2] = "Mar&ccedil;o"; 
	NomeMes[3] = "Abril"; NomeMes[4] = "Maio"; NomeMes[5] = "Junho"; NomeMes[6] = "Julho";
	NomeMes[7] = "Agosto"; NomeMes[8] = "Setembro"; NomeMes[9] = "Outubro";
	NomeMes[10] = "Novembro"; NomeMes[11] = "Dezembro"
	document.write ("<font size=1><B>" + NomeDia[dias] + "</B>, " + dia + " de " + NomeMes[mes] + " de " + ano + "</font>")
}//WriteDate()

function FormatMoney(valor){
	valor = trim(valor).toString();
	valor_temp = replace(replace(valor, ".", ""), ",", "");
	//se encontrar ponto e virgula ou não for uma valor númerico...
	if ((valor_temp.indexOf(";") != -1) || (isNaN(valor_temp))){
		return "0,00";
	}else {
		var residuo = 0;
		//nem ponto nem virgula
		if (valor.indexOf(".") == -1 && valor.indexOf(",") == -1){
			valor = valor + ",00";
		//só ponto
		} else if (valor.indexOf(".") != -1 && valor.indexOf(",") == -1){
			valor = replace(valor, ".", ",");
		//ponto e virgula
		} else if (valor.indexOf(".") != -1 && valor.indexOf(",") != -1){
			valor = replace(valor, ".", "");
		//se tiver mais de uma virgula ou mais de um ponto
		}//if
		if ((valor.indexOf(",") != valor.lastIndexOf(",")) || (valor.indexOf(".") != valor.lastIndexOf("."))){
			var virgula = parseInt(valor.lastIndexOf(","));
			var ponto = parseInt(valor.lastIndexOf("."));
			if (virgula > ponto){
				var inteiro = valor.substr(0, virgula);
				var decimal = valor.substr(virgula+1);
			} else {
				var inteiro = valor.substr(0, ponto);
				var decimal = valor.substr(ponto+1);
			}//if
			inteiro = replace(inteiro, ".", "");
			inteiro = replace(inteiro, ",", "");
			decimal = replace(decimal, ".", "");
			decimal = replace(decimal, ",", "");
			valor = inteiro+","+decimal;
		}//if
		var inteiro = valor.substr(0, valor.indexOf(","));
		var decimal = valor.substr(valor.indexOf(",")+1);
		if (decimal.length > 2)	{
			decimal = decimal.substr(0, 2)+'.'+decimal.substr(2);			
			decimal = Math.round(decimal)+'';
			if (decimal == "100"){
				residuo = 1;
				decimal = "00"
			}//if
			if (decimal.length == 1){
				decimal = "0" + decimal;
			}//if
		}//if
		if (decimal.length == 0) {
			decimal = "00";
		} else if (decimal.length == 1) {
			decimal = decimal + "0";
		} else if (decimal.length > 2) {
			decimal = decimal.substr(0, 2);
		}//if
		if (inteiro.length == 0) {
			if (residuo == 0) {
				return "0," + decimal;
			} else {
				return residuo + "," + decimal;
			}//of
		} else {
			var i, retorno;
			var subString;
			for (i = 0; i <= inteiro.length; i++){
				subString = inteiro.length - 3;
				if (subString < 0){
					subString = 0;
				}//if
				if (i == 0){
					if (residuo == 1)
						inteiro = (parseInt(inteiro)+1)+'';
					retorno = inteiro.substr(subString) + "," + decimal;
					inteiro = inteiro.substr(0, subString);
				}else if (inteiro.length >= 3) {
					retorno = inteiro.substr(subString) + "." + retorno;
					inteiro = inteiro.substr(0, subString);
				}else{
					retorno = inteiro + "." + retorno;
					break;
				}//if
				i = 0;
			}//for
			return retorno;
		}//if
	}//if
}//FormatMoney

function FormatBanco(valor)
{
	var retorno = trim(valor).toString();
	if (retorno != '')
	{
		retorno = replace(retorno, ".", "");
		retorno = replace(retorno, ",", ".");
	}//if
	return retorno
}//FormatBanco

function isNumeric(input){
	Campo = trim(input.value);
	if (Campo != "") {
		Contador = 0;
		var TotalCar = Campo.length
		while (Contador < TotalCar) {
			var Char = Campo.toLowerCase().charCodeAt(Contador)
			if ((Char < 48) || (Char > 57)) {
				return false;
				break;
			}//if
			Contador++;
		}//while
	}//if
	return true;
}//isNumeric

function isMoney(input){
	Campo = trim(input.value);
	if (Campo != "") {
		if (isNaN(replace(replace(Campo, ".", ""), ",", ""))){
			return false;
		}//if
	}//if
	return true;
}//isMoney

function Formata_Valores(campo, tipo) {
	Campo = trim(campo.value).toString();
	if (Campo.length > 0){
		if (tipo == '$'){
			if (isNaN(replace(replace(Campo, ".", ""), ",", ""))){
				var campodesc;
				if(campo.form.ROTINAS_MSGNUMERIC){
					campodesc=campo.form.ROTINAS_MSGNUMERIC.value;
				}else{
					campodesc='O valor deste campo precisa ser numérico.';
				}//if
				alert(campodesc);
				campo.focus();
				return false;
			}else{
				campo.value = FormatMoney(Campo);
			}//if
		} else if (tipo == 'N'){
			if (Campo != '') {
				Contador = 0;
				var TotalCar = Campo.length
				while (Contador < TotalCar) {
					var Char = Campo.toLowerCase().charCodeAt(Contador)
					if ((Char < 48) || (Char > 57)) {
						var campodesc;
						if(campo.form.ROTINAS_MSGNUMERIC){
							campodesc=campo.form.ROTINAS_MSGNUMERIC.value;
						}else{
							campodesc='O valor deste campo precisa ser numérico.';
						}//if
						alert(campodesc);
						campo.focus();
						return false;
						break;
					}//if
					Contador++;
				}//while
			}//if
		}//if
	}//if
	return true;
}//Formata_Valores


	function isDate(campo)
	{
		var gField = eval('document.form.'+ campo);
		var inputStr = gField.value;

		// convert hyphen delimiters to slashes
		while (inputStr.indexOf("-") != -1) inputStr = replaceString(inputStr,"-","/");
		var delim1 = inputStr.indexOf("/");
		var delim2 = inputStr.lastIndexOf("/");
		if (delim1 != -1 && delim1 == delim2) return false;
		if (delim1 != -1)
		{
			// there are delimiters; extract component values
			var dd = parseInt(inputStr.substring(0,delim1),10);
			var mm = parseInt(inputStr.substring(delim1 + 1,delim2),10);
			var yyyy = parseInt(inputStr.substring(delim2 + 1, inputStr.length),10);
		}
		else
		{
			// there are no delimiters; extract component values
			var dd = parseInt(inputStr.substring(0,2),10);
			var mm = parseInt(inputStr.substring(2,4),10);
			var yyyy = parseInt(inputStr.substring(4,inputStr.length),10);
		}


		if (isNaN(mm) || isNaN(dd) || isNaN(yyyy))
		{
			// there is a non-numeric character in one of the component values
			return false;
		}
		if (mm < 1 || mm > 12)
		{
			// month value is not 1 thru 12
			return false;
		}
		if (dd < 1 || dd > 31)
		{
			// date value is not 1 thru 31
			return false;
		}
	
		// validate year, allowing for checks between year ranges
		// passed as parameters from other validation functions
		if (('' + yyyy).length == 2)
		{
			// entered value is two digits, which we allow for 1930-2029
			if (yyyy >= 30)
			{
				yyyy += 1900;
				gField.value = yyyy;
			}
			else
			{
				yyyy += 2000;
			}
		}
		else
		{
			if (('' + yyyy).length == 4)
			{
				if (yyyy > 2029 || yyyy < 1930) return false;
			}
			else
			{
				return false;
			}
		}
	
		if (!checkMonthLength(mm,dd)) return false;
		if (mm == 2)
		{
			if (!checkLeapMonth(mm,dd,yyyy)) return false;
		}
		return true;
	}

	function checkLeapMonth(mm,dd,yyyy)
	{
		if (yyyy % 4 > 0 && dd > 28) return false;
		else if (dd > 29) return false;

		return true;
	}

	function checkMonthLength(mm,dd)
	{
		var months = new Array("","Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
		if ((mm == 4 || mm == 6 || mm == 9 || mm == 11) && dd > 30) return false;
		else if (dd > 31) return false;
		return true;
	}

function ajaxInit() {
	var req;
	try {
		req = new ActiveXObject("Microsoft.XMLHTTP");
	} catch(e) {
		try {
			req = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(ex) {
			try {
				req = new XMLHttpRequest();
			} catch(exc) {
				alert("Esse browser não tem recursos para uso do Ajax");
				req = null;
			}//try
		}//try
	}//try
	return req;
}//ajaxInit

function valida_form(forms){
	valida_campos(forms);
}//valida_form

function checkRadio(objRadio, valor){
	if (objRadio.length > 1){
		for (var i=0; i<objRadio.length; i++){
			if (objRadio[i].value == valor){
				objRadio[i].checked = true;
				break;
			}//if
		}//for
	}else{
		if (objRadio.value == valor){
			objRadio.checked = true;
		}//if
	}//if
}//checkRadio

function checkSelect(objSelect, valor){
	for (var i=0; i<objSelect.options.length; i++){
		if (objSelect.options[i].value == valor){
			objSelect.options[i].selected = true;
		}//if
	}//for
}//checkSelect

function inSelect(obj, valor){
	var ret = false;
	var i;
	for (i=0; i<obj.options.length; i++){
		if (obj.options[i].value == valor){
			ret = true;
			break;
		}//if	
	}//for
	return ret;
}//inSelect

function radioValue(objRadio){
	var valor = "";
	if (objRadio.length > 1){
		for (var i=0; i<objRadio.length; i++){
			if (objRadio[i].checked){
				valor = objRadio[i].value;
				break;
			}//if
		}//for
	}else{
		if (objRadio.checked){
			valor = objRadio.value;
		}//if
	}//if
}//radioValue