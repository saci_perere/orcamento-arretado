* Orçamento Arretado

  Projeto de mineração, organização e análise de dados públicos do orçamento da cidade do Recife com o objetivo de construir uma proposta alternativa de orçamento anual.

* Dependências:
 
Anaconda  - https://www.anaconda.com/distribution/
Tabula-py - https://github.com/chezou/tabula-py