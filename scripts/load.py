import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns       
import glob

pd.options.display.max_columns = None
sns.set(style="ticks")
p=['-o','-o','-o','-o','-o','-o','-o','-o','-o','-o',\
  '-x','-x','-x','-x','-x','-x','-x','-x','-x','-x',\
  '-.','-.','-.','-.','-.','-.','-.','-.','-.','-.',
  '--','--','--','--','--','--','--','--','--','--',\
  's-','s-','s-','s-','s-','s-','s-','s-','s-','s-']

li = [] 
path = '../../data/dataset'
all_files = glob.glob(path + "/*.csv")
for filename in all_files:
    print(filename)
    df = pd.read_csv(filename, sep=';', decimal=',')
    li.append(df)
frame = pd.concat(li, ignore_index=True)
PK = ['orgao_nome','unidade_nome', 'empenho_ano', 'empenho_numero', 'subempenho']
total = len(frame)

#transforma Valores em tipo float64
frame[['valor_empenhado', 'valor_liquidado', 'valor_pago']] = \
frame[['valor_empenhado', 'valor_liquidado', 'valor_pago']].astype('float')

df = frame

#dropa os codigos
df.drop(df.filter(regex='codigo$', axis=1).axes[1], axis=1, inplace=True)
#df.drop('credor_nome', axis=1, inplace=True)

#mantem os nomes como categorias
#df1[df1.filter(regex='nome$', axis=1).axes[1]] = df1[df1.filter(regex='nome$', axis=1).axes[1]].astype('category')
#= df1[df1.filter(regex='nome$', axis=1).axes[1]].astype('category')

df.loc[df.valor_liquidado > 0, 'liquidado'] = 1
df.loc[df.valor_liquidado < 0, 'liquidado'] = -1
df.loc[df.valor_liquidado == 0, 'liquidado'] = 0
df.loc[df.valor_pago > 0, 'pago'] = 1
df.loc[df.valor_pago < 0, 'pago'] = -1
df.loc[df.valor_pago == 0, 'pago'] = False
df.loc[df.valor_empenhado != 0, 'empenhado'] = True
df.loc[df.valor_empenhado == 0, 'empenhado'] = False

valores = ['valor_pago', 'valor_liquidado']



print(df.info())
print("\n#######\nDataFrame origianl:\tframe")
print ("DataFrame para uso:\tdf")
print("Tupla PK\t" + str(PK)+ "\n#######\n")




