#!/bin/python3

from bs4 import BeautifulSoup
import re
import os
with open("../data/html/Portal da Transparência - PCR.html") as fp:
    soup = BeautifulSoup(fp)
    a = re.compile("^http://transparencia.recife.pe.gov.br/uploads/pdf/Propaganda%20Institucional")
    b = re.compile("^http://transparencia.recife.pe.gov.br/uploads/pdf/Comunica%C3%A7%C3%A3o%20Institucional")
    c = re.compile("^http://transparencia.recife.pe.gov.br/uploads/pdf/Publicidade%20")
    d = re.compile("^http://transparencia.recife.pe.gov.br/uploads/pdf/Despesas%20de%20Publicidade")
    #d = re.compile("^http://transparencia.recife.pe.gov.br/uploads/pdf")

    pattern = [a,b,c,d] 
#   pattern = re.compile("^http")
    #print (pattern[0])
    c=0
    for link in soup.find_all('a'):
      #  print(link.get('href'))
        if (link.get('href') != None):
            for x in pattern:
                a = x.match(link.get('href'))
                #print (a)
                if (a != None):
                    c += 1
                    print(link.get('href'))
                    os.system("wget " + link.get('href'))
                    os.system("mv *.pdf ../data/pdf/")
