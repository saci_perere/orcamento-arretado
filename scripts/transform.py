import pandas as pd
#import matplotlib.pyplot as plt
#import numpy as np
#import seaborn as sns       
#import glob

def agrupa_atrib (df, attr):
    '''
    Recebe:   Dataframe e Atributo a ser analisado
    Retorna:  Novo DataFrame agrupado em torno do Atributo
    ''' 
    return df.groupby(['ano_movimentacao', attr])\
            [['valor_pago', 'valor_liquidado']].sum().unstack().fillna(0)
    

def ordem_grandeza (df, c, i):
    '''
    Recebe:   DataFrame, char e valores
    Retorna:  Alteração direta no DataFrame, com nova escala de grandeza dos valores
    '''
    dic_ord = {'b': 1000000000, 'm': 1000000, 'k': 1000, 'u': 1}
    df[i] = df[i] / dic_ord[c]

        




def anota(df, plot):
    for i, point in df.iterrows():
        for pp in point.array:
            pt = pp
            plot.annotate("%.2f" % pt, (i, pp))

def nomeGrandeza(grandeza):
    dic_ord = {'b': 'Bilhões', 'm': 'Milhões', 'k': 'Milhares', 'u': 'Unidades'}
    return dic_ord[grandeza]
    

            
def grafico (df, nome1, titulo, \
             log=True, figSize=(20,12), \
             grandeza = 'm',\
             move_legenda = False,\
             valores = ['valor_pago', 'valor_liquidado'],\
             nomex ='Ano de Movimentação'):      

    for y in valores:

        ordem_grandeza(df, grandeza, y)

        ll = [False]
        if (log):
            ll.append(True)
            
        for l in ll:
            
            #unidade, mil, milhão, bilhão: 'u','k','m','b'
            c = ('linear' if (l == False) else 'log')

            #nome = 'anoMovimentacao_' + y + '_grupoINVESTIMENTO_funcao_' + c    
            nome = nome1[0] + y + nome1[1] + c
            
            if (not l):
                df[y].to_csv('../../data/analise/'+nome+'.csv', \
                        sep=';', decimal=',', float_format='%.2f')
            
            plot = df[y].plot(figsize=figSize, style=p, \
#                             title="Distribuição por Função/nDentro do grupo INVETIMENTOS", \
                             title=titulo, \
                              logy=l,\
                              legend=not move_legenda)
            
            # Função para gerar etiquetas
            
            if (not l):
                anota(df[y], plot)
                plot.ticklabel_format(style='plain')


            if (move_legenda):
                fig = plot.get_figure()
                fig.legend(loc='center left', bbox_to_anchor=(0, 0.4))
            
            plt.xlabel(nomex, fontsize=14)
            plt.ylabel(y + '\n em ' + \
                       nomeGrandeza(grandeza) + \
                       ' de R$', fontsize=14)
            
            fig = plot.get_figure()
            fig.savefig('../../plots/'+nome)
 
