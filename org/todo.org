* Tarefas:
** DONE Mudar as referências das pastas/arquivos
   CLOSED: [2020-03-11 qua 21:05]
** TODO Pesquisar Referências
** Fazer a correção dos valores pelo IPCA para os outros anos passados
** Responder às [[./docs/reuniao-09-03-2020-perguntas][Perguntas]][3/7]
*** TODO Correção IPCA
*** DONE PERGUNTA 1: Série histórica por fontes
    CLOSED: [2020-03-19 qui 19:37]
    OBS: Análise do comportamento da receita na Introdução.
*** TODO PERGUNTA 2: Como estão distribuídos por unidade os pagamentos de credores por sub-elemento? Concentração em CNPJs.
Não cheguei a uma visualização adequada porém os dados literais foram indicados
*** DONE PERGUNTA 3: Série histórica das despesas com o GRUPO Investimentos.
    CLOSED: [2020-04-07 ter 17:11]
*** DONE PERGUNTA 4: Distribuição das despesas com o GRUPO Investimentos por Função / Subfunção.
    CLOSED: [2020-04-07 ter 17:11]
*** TODO PERGUNTA 5: Distribuição dos gastos com publicidade:[0/6]
Problemas na extração dos dados/ chocar com o que eles tem no mandato / automatizar
**** TODO Organizar extração html>pdf
**** TODO Organizar extração pdf>tabela
**** TODO por Ano
**** TODO por Credor / Agência
**** TODO por Subcontratado
**** TODO por Tipo de Campanha
*** TODO PERGUNTA ESPECIAL: DESAFIO DE ATUALIZAÇÃO DO ANO CORRENTE
**** Desafio prático do dia-a-dia dos gastos cumulativos pro exercício corrente:
***** Função / SubFunção
***** Projeto / Atividade
***** Elemento / Sub-elemento
**** 2020 entra
**** Discutir atualização do ano de 2020

** Distribuição por Fontes [0/4]
   Não vamos abordar:
   0249 RECURSOS PRÓPRIOS DO SIST. PREVIDENCIÁRIO DO MUNICÍPIO
   0250 RECURSOS PRÓPRIOS DO SAÚDE - RECIFE
*** TODO Grupo 1 [0/3]
    0100 RECURSOS ORDINÁRIOS - NÃO VINCULADOS
    0111 RECURSOS DA DESVINCULAÇÃO DE RECEITAS - DREM-EC93/2016
    0112 EDUCAÇÃO - COMPL. LIMITE CONSTITUCIONAL
    0113 TRANSFERÊNCIAS DO FUNDEB
    0114 SAÚDE - LIMITE CONSTITUCIONAL
    0117 TRANSFERÊNCIAS DO FDS
    0120 RECURSOS DE MULTAS DE TRÂNSITO
    (contrapartidas e convênios)
    0102 CONVÊNIOS E ACORDOS A FUNDO PERDIDO (PREFEITURA RECIFE)
    0103 OPERAÇÕES DE CRÉDITO
    0104 TRANSFERÊNCIAS DO FNDE
    0105 TRANSFERÊNCIA FNDE (SALÁRIO - EDUCAÇÃO)
    0106 OPERAÇÕES DE CRÉDITO - SWAP EDUCAÇÃO
    0109 OPERAÇÕES DE CRÉDITO - CPAC
    0133 CONTRAPARTIDA DE CONVÊNIO
    0142 CONTRAPARTIDA DE CONV. - EDUCAÇÃO - COMPL. LIM. CONST.
    0143 CONTRAPARTIDA DE CONVÊNIO - CIP
    0144 CONTRAPARTIDA DE CONVÊNIO - SAÚDE - LIM. CONSTITUCIONAL
    4103 CONTRAPARTIDA OPERAÇÃO DE CRÉDITO DIVERSAS ( CEF)
    0243 TRANSFERÊNCIAS DO FEAS
    0244 TRANSFERÊNCIAS DO SUS
    0245 TRANSFERÊNCIAS DO FNAS
    0247 TRANSFERÊNCIAS DO FUNDO NACIONAL DE CULTURA
    0248 TRANSF DO FUNDO ESTADUAL DE APOIO E DESENV. MUNICIPAL
**** TODO Função / Subfunção
**** TODO Projetos / Atividades
***** TODO Elemento / Subelemento de Despesa
**** TODO Elemento / Subelemento de Despesa
*** TODO Grupo 2 [0/2]
    0116 TRANSFERÊNCIAS DA CIDE
    0125 CMR - RECURSOS DO LIMITE CONSTITUCIONAL
    0115 SAÚDE - VIGILÂNCIA SANITÁRIA
**** TODO Projetos / Atividades
**** TODO Elemento / Subelemento de Despesa
*** TODO Grupo 3 [0/2]
    0126 TRANSFERÊNCIA DE DOAÇÕES - INCENTIVOS FISCAIS DA UNIÃO
    0129 RECURSOS DE INCREMENTO DA ARRECADAÇÃO TRIBUTÁRIA
**** TODO Função / Subfunção
**** TODO Elemento / Subelemento de Despesa
*** TODO Grupo 4 [0/5]
    0241 RECURSOS PRÓPRIOS (ADM. SUPERVISIONADA)
    0242 CONVÊNIOS E ACORDOS A FUNDO PERDIDO (ADM. SUPERV.)
    0246 INCENTIVOS FISCAIS E DOAÇÕES
**** TODO Órgão / Unidade
**** TODO Função / Subfunção
**** TODO Projetos / Atividades
***** TODO Elemento / Subelemento de Despesa
**** TODO Elemento / Subelemento de Despesa
** TODO Montar script que unifica da extração à análise.

* Observações:
** desagragação dos dados
- já é possível ler as informações através de um recorte escolhido.
  uma das possíveis implementações poderá ser um mecanismo de desagragação,
  por exemplo: retirar o que não poderia ser alterado numa LOA alternativa
** optar entre ter os dados abertos na forma CSV ou raspando as Consultas
*** cada um pode servir a diferentes propósitos
*** data exata existe na consulta
*** existem dados que não estão nas consultas
** Quais são as regras de negócio para a criação de uma LOA alternativa
*** O que não pode mexer
*** Quais são as assunções estruturais 
- (ex: não haverá mudança no quadro de contratações)
** Dotação orçamentaria é dado ao nível de CATEGORIA>GRUPO>ELEMENTO
** Restos pagos e Totais pagos
** Visões do portal da transparência
- Fonte / Modalidade
  + Dotação Inicial, Dotação Atualizada, Empenhado, Liquidado, Pago, P/E
    - Categoria Econômica
      - Grupo
        - Elemento
          - Subelemento

- Órgão / Unidade / Fonte / Modalidade
  + Dotação Inicial, Dotação Atualizada, Empenhado, Liquidado, Pago, pago/empenhado
    - Órgão
      - Unidade
        - Categoria Econômica
          - Grupo
            - Elemento
              - Subelemento

- Órgão / Unidade / Modalidade / CNPJ / Credor / No Emepenho 
  + Tabela: (além dos dados do filtro) | empenho | modalidade do empenho | 
tipo de licitação | empenhado | liquidado | pago | data pgto

- Função / Subfunção / Programa / Ação / Fonte (Funcional programático)
  + Tabela: (além dos dados do filtro) | dotação inicial | dotação atualizada | 
empenhado | liquidado | pago | pago/empenhado

- Órgão executor / Órgão concedente (Convênios)
  + Ano, Número, Órgão Executor, Órgão Concedente, Objeto,
Data início de vigência, Data término de vigência, Data de celebração,
Data limite de prestação de contas, Valor pactuado, Valor de contrapartida,
Valor global

** MSG para Breno

Então eu tô com algumas dúvidas do processo que fizemos até aqui de explorar o 
CSV disponibilizado no Dados Abertos. Porém a gente meio que deu uma recuada para 
fazer uma análise melhor tanto desses dados quanto de outras visões oferecidas 
pelo portal de transparência.

só que a preferência é sempre trabalhas com o CSV direto do que ter que 
processar as consultas e raspar os dados de lá

Algumas informações não bateram com o que puxamos lá da visão de Consulta Geral.

Aí queria saber se isso já foi notado, existe alguma explicação? Ou vocês 
desconhecem isso, se for o caso posso reprocessar essas inconcistencia e te 
apresentar direitinho.

Aí uma dúvida é:
- As páginas de consultas, bebem da mesma base de dados fornecida no Dados Abertos?
- Em caso negativo: De que outras fontes ela se alimenta?

Outras dúvidas:
- O que significam valores de empenho, liquidação e pagamento negativos?
- Consideramos (ORGAO-ANO-EMPENHO-SUBEMPENHO), como uma chave única, porém observamos linhas com esta mesma "chave".
— Há outros campos que não consideramos nessa chave, que tornariam um empenho único?
— Qual a semântica dessas repetições? Substituição do empenho original, por exemplo? 
Se sim, deveriamos assumir o empenho mais recente como o válido?

Tamos fazendo uma revisão aqui, como disse acima. Essas foram dúvidas do processo 
de leitura inicial das bases de dados. Aí André me indicou que tu poderia me 
indicar uma nitidez maior nessas duvidas, caso não seja possível, por que caminhos 
ou que pessoas tu me encaminharia para tirar essas e outras dúvidas do tipo?

* Reuniao 31032020
** Django p construção do DB fluiu
** Construir uma argumentação sobre as inconscistências
*** procurar casos
** ETL
** Análise
** Passar materiais para eles poderem criar novas
** Tarefas: [0/5] 
*** TODO Contrução de BD
    SCHEDULED: <2020-04-07 ter>
**** TODO Responder às perguntas | Validar o BD
*** TODO Procurar Inconscistências
*** TODO Organizar GIT
*** TODO Extração
**** TODO Propaganda
**** TODO Dotação Orçamentária
** Horários 
*** Terça / Quarta
*** 14h - 18h


* Reuniao 14042020
   - delimitador: ';'
   - Valor 'xxx,xx'
   - pontuar no gráfico
   - arrendondar valores
   - gráficos são genéricos, apresentação irá especializar para publico externo
